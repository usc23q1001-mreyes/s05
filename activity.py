import abc

class Animal(metaclass=abc.ABCMeta):
  @abc.abstractmethod
  def eat(self, food):
    pass
  
  @abc.abstractmethod
  def make_sound(self):
    pass


class Cat(Animal):
  def __init__(self, name, breed, age):
    self._name = name
    self._breed = breed
    self._age = age

  def eat(self, food):
    print(f'{self._name}: {food} yummy yummy more')
  def make_sound(self):
    print(f'{self._name}: meow meow, Kneel before me Human! meow 😸😸')
  def call(self):
    print('meeeng meeng ali meengg')

  # getters
  def get_name(self):
    return self._name
  def get_breed(self):
    return self._breed
  def get_age(self):
    return self._age
  
  # settters
  def set_name(self, name):
    self._name = name
  def set_breed(self, breed):
    self._breed = breed
  def set_age(self, age):
    self._age = age
  
class Dog(Animal):
  def __init__(self, name, breed, age):
    self._name = name
    self._breed = breed
    self._age = age

  def eat(self, food):
    print(f'{self._name}: {food} soo goood 🐶🐶')
  def make_sound(self):
    print(f'{self._name}: Arf Arf 🐕🐕')
  def call(self):
    print('doggy doggyyyy come hiyaaa')

  # getters
  def get_name(self):
    return self._name
  def get_breed(self):
    return self._breed
  def get_age(self):
    return self._age
  
  # settters
  def set_name(self, name):
    self._name = name
  def set_breed(self, breed):
    self._breed = breed
  def set_age(self, age):
    self._age = age


cat = Cat('memeng', 'orange cat 😽', 3)
cat.eat("Tuna")
cat.make_sound()
cat.call()

dog = Dog('Snoop Dogg', 'rendon labrador', 3)
dog.eat("Lechon Manok")
dog.make_sound()
dog.call()